var client = require('./connection');


const createIndex = () =>
{
    client.indices.create({  
        index: 'yuvaltest'
      },function(err,resp,status) {
        if(err) {
          console.log(err);
        }
        else {
          console.log("create",resp);
        }
      });
}

async function isExsit(username){
    const res = await client.search({
        index: 'yuvaltest',
        type: 'user',
        body: {
          query: {
            match: { "Username": username}
          }
        }
      });
    if(Number(res.body.hits.total) > 0){
        return true;
    }
    else {
        return false;
    }
    
}

async function getLastId(){
    const res = await client.search({
        index: 'yuvaltest',
        type:'user',
        body: {
            query:{
                match_all: {}
            },
            sort:[{ "_id": { "order": "desc" } }]
        }
    });
    if(res.body.hits.hits){
        console.log( res.body.hits.hits[0]._id);
        return Number(res.body.hits.hits[0]._id);
    }
    else{
        return 0;
    }
}

async function register(username,password,email,name) {
    
    let Exsist=true;
    // const isexsit = isExsit(username).then();
    const isexsit = await isExsit(username).then(function(resp){
        Exsist = resp;
    });
    if(Exsist){
        return false;
    }
    else{
        console.log("getting last")
        let id;
        const lastId = await getLastId().then (function(resp){
            id=resp+1
            const create = client.index({  
                index: 'yuvaltest',
                id: id,
                type: 'user',
                body: {
                "Username": username,
                "Password": password,
                "Name": name,
                "Email": email
            }
          },function(err,resp,status) {
              if(err) console.log(err);
          });
          console.log("Register new user - " , username)
        });
        return true;
    }

    

}

async function login  (username, password) {
    const resp = await client.search({  
        index: 'yuvaltest',
        type: 'user',
        body: {
          query: {
            match: { "Username": username}
          }
        }
      });
    if(resp.body.hits.hits[0]){
        if(resp.body.hits.hits[0]._source.Password == password){
            console.log("new login - " , username)
            return true;
        }
    }

      
      
    //   .then(function(resp) {
       
    // }, function(err) {
    //     console.trace(err.message);
    //     return false;
    // });
}


exports.register = register;
exports.login = login;