const express = require('express');
const app = express();

const expHBS = require('express-handlebars');
app.engine('handlebars', expHBS());
app.set('view engine', 'handlebars');

const bodyParser = require('body-parser');
const router = express.Router();
const validation = require('express-validation');
const postValidation = require('./validation');
const regisValidation = require('./regValidation');
const logValidation = require('./logValidation');
const es = require('./elastic');
var cookieParser = require('cookie-parser');


app.use(cookieParser());

app.use((req,res,next) => {
    // console.log('middle works');
    next();
});

app.use(bodyParser.urlencoded({
    extended : true
}));

app.use(bodyParser.json());


app.use('/', router);

app.use((err,req,res,next) =>{
    if(err instanceof validation.ValidationError){
        if(req.url == '/register'){
            res.render('register',{
                problems: err
            });
        }
        else if(req.url == '/login'){
            res.render('login',{
                problems: err
            });
        }

    }
});

router.route('/').get((req,res) =>  {
    console.log(req.cookies);
    if(req.cookies.userExpress === undefined){
        res.redirect('/login');
    }
    else{
        res.redirect('/home');
    }
});

router.route('/register').get((req,res) => {
    if(req.cookies.userExpress === undefined){

        res.render('register', {
            pageTitle : 'Register',
        });
    }
    else{
        res.redirect('/');
    }
});

router.route('/register').post(validation(regisValidation),(req,res) => {
    const register = es.register(req.body.username, req.body.password, req.body.email, req.body.name).then(function(resp){
        if(resp){
            res.redirect('/login');
        }
        else{
            res.render('register', {
                problems : 'Username allready exsit',
            });
        }
    });
    
});

router.route('/login').post(validation(logValidation),(req,res) => {
    es.login(req.body.username, req.body.password).then(function(resp){
        if(resp){
            res.cookie("userExpress", req.body.username); 
            res.redirect('/home');
        }
        else{
            res.render('login', {
                problems : 'Username or Password incurrect'
            });
        }
    })
});
router.route('/login').get((req,res)=>{
    if(req.cookies.userExpress === undefined){
        res.render('login');
    }
    else{
        res.redirect('/');
    }
    
});
router.route('/home').get((req,res)=>{
    if(req.cookies.userExpress === undefined){
        res.redirect('/');
    }
    else{
        res.render('home', {
            user : req.cookies.userExpress
        });
    }

});

router.route('/logout').get((req,res)=>{
    res.clearCookie('userExpress');
    res.redirect('/login');
});

app.listen(33322, () => {
    console.log('Started');
});

