const joi = require('joi');

module.exports = {
    body: {
        username: joi.string().alphanum().min(3).max(30).required(),
        password: joi.string().alphanum().min(3).max(30).required(),
    }
}