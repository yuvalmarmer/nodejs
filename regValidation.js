const joi = require('joi');

module.exports = {
    body: {
        name: joi.string().alphanum().min(3).max(30).required(),
        username: joi.string().alphanum().min(3).max(30).required(),
        password: joi.string().alphanum().min(3).max(30).required(),
        email: joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    }
}